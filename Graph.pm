package Goo::Graph::Time;
use 5.10.0;
use Carp;
use Mouse;

use Goo::Canvas;

use constant X => 0;
use constant Y => 1;

my $black = Gtk2::Gdk::Color->new (0x0000,0x0000,0x0000);
my $white = Gtk2::Gdk::Color->new (0xFFFF,0xFFFF,0xFFFF);

my $margin = 80;

has 'canvas' => (
	is      => 'ro',
	isa     => 'Goo::Canvas',
	lazy    => 1,
	builder => '_build_canvas',
);

sub _build_canvas {
	my ($self) = @_;
	my $this = Goo::Canvas->new();
	$this->set_bounds(0, 0, $self->canvas_size->[X], $self->canvas_size->[Y]);
	$this->modify_base('normal', $white);
	$this;
}

has 'canvas_size' => (
	is      => 'ro',
	isa     => 'ArrayRef',
	lazy    => 1,
	builder => '_build_canvas_size',
);

sub _build_canvas_size {[ 600, 600 ]}

has 'plot_size' => (
	is      => 'ro',
	isa     => 'ArrayRef',
	lazy    => 1,
	builder => '_build_plot_size'
);

sub _build_plot_size {
	my ($self) = @_; 
	[
		$self->canvas_size->[X] - $margin, 
		$self->canvas_size->[Y] - $margin
	]
};

has ['x_label', 'y_label'] => (
	is       => 'ro',
	required => 1,
);

#The x axis range, in seconds.
#Negative values give relative scale. 
# ie [-600, 0] gives a graph from now to -10 minutes
# [1000, 2000] gives a graph from epoch time 1000 -> 2000

has 'x_range' => (
	is       => 'rw',
	isa      => 'ArrayRef[Int]',
	trigger  => \&_x_range_changed,
	required => 1,
);

has 'y_range' => (
	is       => 'rw',
	isa      => 'ArrayRef[Int]',
	trigger  => \&_y_range_changed,
	required => 1,
);

has 'axis_layer' => (
	is      => 'rw',
	lazy    => 1,
	builder => '_build_axis_layer',
);

sub _build_axis_layer {
	my ($self) = @_;
	Goo::Canvas::Group->new($self->canvas->get_root_item);
}

has ['new_line_layer', 'old_line_layer'] => (
	is      => 'rw',
#	lazy    => 1,
	builder => '_build_line_layer',
);

sub _build_line_layer {
	my ($self) = shift;
	my $group = Goo::Canvas::Group->new($self->canvas->get_root_item);
	$group->scale(1,-1);
	$group->translate($self->canvas_size->[X], -$self->canvas_size->[Y] + $margin);
	$group->lower($self->axis_layer);
	$group;
}

sub add_points {
	state @last_point;
	state $zero_time  = time;
	state $last_time  = $zero_time;
	state $time_shift = 0;
	
	my ($self, @new_points) = @_;
	my $num_new_points = @new_points;
	croak "Uneven number of points given" if $num_new_points % 2;
	
	my $x_point_range = abs($self->x_range->[0] - $self->x_range->[1]);
	my $y_point_range = abs($self->y_range->[0] - $self->y_range->[1]);
	
	my $x_point_conv = ($self->plot_size->[X] / $x_point_range);
	my $y_point_conv = ($self->plot_size->[Y] / $y_point_range);
	
	#Normalize the values
	for my $i (0 .. $num_new_points/2 - 1) {
		#For X
		$new_points[$i*2] = 
			($new_points[$i*2] - $zero_time) * $x_point_conv;
		
		#For Y
		$new_points[$i*2+1] = 
			($new_points[$i*2+1] - $self->y_range->[0]) * $y_point_conv;
	}
	
	#Get the 'size' of the new data. To see how far we need to push the graph over.
	my $now = time;
	my $x_gap = ($now - $last_time) * $x_point_conv;
	$last_time = $now;
	
	if ($x_gap >= $x_point_range) {
		#Just clear off everything.
		$self->_remove_group($self->old_line_layer);
		$self->_remove_group($self->new_line_layer);
		$self->old_line_layer($self->_build_line_layer);
		$self->new_line_layer($self->_build_line_layer);
		
		#Sweep the point list for what we can show.
		my ($i, $j);
		for $j (0 .. $num_new_points/2 - 1) {
			$i = $j && last 
				if $new_points[$j*2] - $new_points[-2] >= -$x_point_range;
		}
		$i -= 2 unless $i == 0;
		splice @new_points, 0, $i;
		
		Goo::Canvas::Polyline->new (
			$self->old_line_layer, 0,
			[@last_point, @new_points],
			'stroke-color' => 'green',
			'line-width'   => 2,
		);
		
		@last_point = @new_points[-2,-1];
	}
	else {
		Goo::Canvas::Polyline->new (
			$self->new_line_layer, 0,
			[@last_point, @new_points],
			'stroke-color' => 'green',
			'line-width'   => 2,
		);
		$self->old_line_layer->translate(-$x_gap, 0);
		$self->new_line_layer->translate(-$x_gap, 0);
		
		if ($now - $zero_time >= $self->plot_size->[X]) {
			#Out with the old
			$self->_remove_group($self->old_line_layer);
			$self->old_line_layer($self->new_line_layer);
			#In with the new.
			$self->new_line_layer($self->_build_line_layer);
			
			#Translate everything to match the new layer
			@last_point = ($new_points[-2] - ($now - $zero_time) * $x_point_conv, $new_points[-1]);
			$zero_time = $now;
		}
		else {
			@last_point = @new_points[-2,-1];
		}
	}
}

sub _remove_group {
	my ($self, $group) = @_;
	my $root = $self->canvas->get_root_item;
	for my $i (0 .. $root->get_n_children-1) {
		if ($group eq $root->get_child($i)) {
			$root->remove_child($i);
			last;
		}
	}
}

#XXX Split up _redraw_axis into these two functions.
#Will need to add an extra group instead of using one axis_layer.
sub _x_range_changed {
	push @_, undef if @_ == 2;
	_redraw_axis(@_, 'x');
}

sub _y_range_changed {
	push @_, undef if @_ == 2;
	_redraw_axis(@_, 'y');
}

sub _redraw_axis {
	my ($self, $updated_val, $orig_val, $axis) = @_;
	my $root = $self->canvas->get_root_item;
	my $old_layer = $self->axis_layer;
	my $axis_layer = Goo::Canvas::Group->new($root);
	$self->axis_layer($axis_layer);
	
	#Remove the old group to quickly clear the old axis.
	$self->_remove_group($old_layer);

	Goo::Canvas::Rect->new(
		$axis_layer, 
		0, $self->canvas_size->[Y]-$margin, 
		$self->canvas_size->[X], $self->canvas_size->[Y],
		'line-width'   => 1,
		'stroke-color' => 'white', 
		'fill-color'   => 'white',
	);
	
	Goo::Canvas::Rect->new(
		$axis_layer, 0, 0, $margin, $self->canvas_size->[Y],
		'line-width'   => 1,
		'stroke-color' => 'white', 
		'fill-color'   => 'white',
	);
	
	my $axis_lines = [
		#Start at the Y axis tip.
		$margin, 0, 
		$margin, $self->canvas_size->[Y]-$margin,
		$self->canvas_size->[X], $self->canvas_size->[Y]-$margin,
	];
	Goo::Canvas::Polyline->new(
		$axis_layer, 0, 
		$axis_lines,
		'stroke-color' => 'black',
		'line-width'   => 3,
	);
	
	my $x_point_range = $self->x_range->[1] - $self->x_range->[0];
	my $x_point_conv  = ($self->plot_size->[X] / $x_point_range);
	my $x_time_range  = $x_point_range*$x_point_conv;
	
	my $ideal_x_divisions = 20;
	my $sec_per_div = $ideal_x_divisions / $x_point_conv;
	my $x_divisions = (int($sec_per_div) + (5 - int($sec_per_div) % 5)) * $x_point_conv;

	for my $step (1 .. $self->plot_size->[X]/$x_divisions) {
		$step *= $x_divisions;
		my $notch = [
			$margin + $step, 
			$self->canvas_size->[Y]-$margin,
			
			$margin + $step, 
			$self->canvas_size->[Y]-$margin+5,
		];
		
		Goo::Canvas::Polyline->new(
			$axis_layer, 0, 
			$notch,
			'stroke-color' => 'black',
			'line-width'   => 2,
		);
		my $val = sprintf('%.0f', ($x_time_range-$step) / $x_point_conv);
		my $markup = "<span font_family ='Arial '
			foreground = '#0000ff'  
			size = '10000'
			weight = 'light'> $val </span>";

		Goo::Canvas::Text->new(
			$axis_layer,
			$markup,
			$step+$margin-3, $self->plot_size->[Y], 1, 
			'north-east',
			'use markup' => 1,
			'wrap' => 'char',
		);
	}
	
	my $y_point_range = $self->y_range->[1] - $self->y_range->[0];
	my $y_point_conv  = ($self->plot_size->[Y] / $y_point_range);
	
	#Hmm how about ~20 px per major division? Sounds good to me...
	my $ideal_divisions = 20;
	#Find the nearest acceptable scale division.
	my $deg_per_div = $ideal_divisions / $y_point_conv;
	my $rounded_div = $self->_round_nearest_temp($deg_per_div);
	my $y_divisions = $rounded_div * $y_point_conv;

	for my $step (1 .. $self->plot_size->[Y]/$y_divisions) {
		$step *= $y_divisions;
		
		my $notch = [
			$margin,
			$self->canvas_size->[Y]-$margin-$step,
			
			$margin-5, 
			$self->canvas_size->[Y]-$margin-$step,
		];
		
		Goo::Canvas::Polyline->new(
			$axis_layer, 0, 
			$notch,
			'stroke-color' => 'black',
			'line-width'   => 2,
		);
		
		my $val = sprintf('%0.2f', $step / $y_point_conv + $self->y_range->[0]);
		my $markup = "<span font_family ='Arial '
			foreground = '#0000ff'  
			size = '10000'
			weight = 'light'> $val </span>";

		Goo::Canvas::Text->new(
			$axis_layer, 
			$markup,
			$margin-50, $self->canvas_size->[Y]-$margin-$step-23, 1, 
			'north-east',
			'use markup' => 1,
		);
	}

	#Now we rescale the line to match the redraw.
	#PS this code is separate from the above code for now.
	if ($axis eq 'x' && defined $orig_val) {
		my $scale = 
			($orig_val->[0] - $orig_val->[1]) / 
			($updated_val->[0] - $updated_val->[1]);
		$self->new_line_layer->simple_transform(0, 0, $scale, 0);
		$self->old_line_layer->simple_transform(0, 0, $scale, 0);
	}
	elsif ($axis eq 'y' && defined $orig_val) {
		my $scale = 
			($orig_val->[0] -$orig_val->[1]) / 
			($updated_val->[0] - $updated_val->[1]);
		$self->new_line_layer->simple_transform(0, 0, $scale, 0);
		$self->old_line_layer->simple_transform(0, 0, $scale, 0);
	}
}


sub _round_nearest_temp {
	my ($self, $gap) = @_;
	#Round to nearest sensible value
	for my $nearest (0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1, 2, 5, 10, 50, 100) {
		return $nearest if $gap < $nearest * 1.1;
	} 
	return 500;
}

__PACKAGE__->meta->make_immutable;
