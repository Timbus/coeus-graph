#!/usr/bin/perl
use 5.10.0;
use Graph;
use Gtk2 '-init';
use AnyEvent::Impl::Glib;
use AnyEvent;
use AnyEvent::Handle;

my $graph = Goo::Graph::Time->new(
	x_label => 'Time (s)', y_label => 'Temperature (Deg C)', 
	x_range => [0, 600], y_range => [0, 100]
);

my $wnd = Gtk2::Window->new();
$wnd->set_type_hint('dialog');
$wnd->set_size_request(600, 600);
$wnd->add($graph->canvas);
$wnd->show_all;



#Set up the server handlery thing.
my $h = AnyEvent::Handle->new(
	connect          => ['localhost', 9999],
	on_connect       => sub { 
		my ($handle, $host, $port, $retry) = @_; 
		say "Connected to Coeus!"; 
	},
	on_connect_error => sub { 
		my ($handle, $message) = @_; 
		say "Could not connect to Coeus!: $message"; 
	},
	on_read          => sub {
		my $fh = shift;
		$fh->push_read(json => \&handle_ret);
	},
);

sub handle_ret {
	my ($fh, $rpc) = @_;
	if ($rpc->{result}){
		my $time = $rpc->{result}{time};
		my $temp = $rpc->{result}{value};
		
		$graph->add_points($time, $temp);
	}
}

my $t = AE::timer 20, 20, sub {
	say "Resizing x";
	$graph->x_range([0, 200]);
};

Gtk2->main;
